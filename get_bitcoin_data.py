# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

import http.client

conn = http.client.HTTPSConnection("bravenewcoin.p.rapidapi.com")

headers = {
    'X-RapidAPI-Key': "e8b084849bmsh343f37208c887aep14de33jsne3c62f3d595f",
    'X-RapidAPI-Host': "bravenewcoin.p.rapidapi.com"
    }

conn.request("GET", "/asset/f1ff77b6-3ab4-4719-9ded-2fc7e71cff1f", headers=headers)

res = conn.getresponse()
data = res.read()


print(data.decode("utf-8"))