from flask import Flask, render_template
import urllib.request, json
import os
import http.client
from fastapi import FastAPI

app = FastAPI()
app = Flask(__name__)


@app.route("/bitcoin")
def get_bitcoin_data():
    headers = {
        'Authorization': "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5EVXhNRGhHT0VReE56STVOelJCTTBJM1FrUTVOa0l4TWtRd1FrSTJSalJFTVRaR1F6QTBOZyJ9.eyJpc3MiOiJodHRwczovL2F1dGguYnJhdmVuZXdjb2luLmNvbS8iLCJzdWIiOiJvQ2RRb1pvSTk2RVJFOUhZM3NRN0ptYkFDZkJmNTVSWUBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9hcGkuYnJhdmVuZXdjb2luLmNvbSIsImlhdCI6MTY2NDIyNjg4MywiZXhwIjoxNjY0MzEzMjgzLCJhenAiOiJvQ2RRb1pvSTk2RVJFOUhZM3NRN0ptYkFDZkJmNTVSWSIsInNjb3BlIjoicmVhZDppbmRleC10aWNrZXIgcmVhZDpyYW5raW5nIHJlYWQ6bXdhIHJlYWQ6Z3dhIHJlYWQ6YWdncmVnYXRlcyByZWFkOm1hcmtldCByZWFkOmFzc2V0IHJlYWQ6b2hsY3YgcmVhZDptYXJrZXQtY2FwIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.E64ZbvqysLHoTI4FqqfQhDeqpsjnL0AGDHfGpA-zQqge34AZJk8dLjHTtpHm3y8iTCdKWOV6DE4xmtvcPMFXZ6__XF4e4oSYX9F7bPfBgklmttx1OmfIAqCW8AbZWcnWOzq1c8zs75Y9icoWhsXKWKVsG8s_EhKyBubPVrhWpjCk0iQypsf2G9HdtRHagDjuBoiplj3-RlXcXN3f3N3INoVVAjVKuZMeDjG1eF1ldI2LBP2rLT3O-0RFXCy2OxQQ5gF2304ZsB_umAFOGAXx3bkvOdksrpYFy4uHiB4Uufs_x4uZYOkaH8CQwiPTsAPKSB_qJz1-43VqCS03Ly0_oQ",
        'X-RapidAPI-Key': "e8b084849bmsh343f37208c887aep14de33jsne3c62f3d595f",
        'X-RapidAPI-Host': "bravenewcoin.p.rapidapi.com"
    }
    conn = http.client.HTTPSConnection("bravenewcoin.p.rapidapi.com")
    conn.request("GET", "/market-cap?assetId=f1ff77b6-3ab4-4719-9ded-2fc7e71cff1f", headers=headers)

    res = conn.getresponse()
    data = res.read()
    print(data.decode("utf-8"))
    return render_template("bitcoin.html", bitcoinData=data.decode("utf-8"))


@app.route("/monero")
def get_monero_data():
    headers = {
        'Authorization': "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5EVXhNRGhHT0VReE56STVOelJCTTBJM1FrUTVOa0l4TWtRd1FrSTJSalJFTVRaR1F6QTBOZyJ9.eyJpc3MiOiJodHRwczovL2F1dGguYnJhdmVuZXdjb2luLmNvbS8iLCJzdWIiOiJvQ2RRb1pvSTk2RVJFOUhZM3NRN0ptYkFDZkJmNTVSWUBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9hcGkuYnJhdmVuZXdjb2luLmNvbSIsImlhdCI6MTY2NDE0OTExMywiZXhwIjoxNjY0MjM1NTEzLCJhenAiOiJvQ2RRb1pvSTk2RVJFOUhZM3NRN0ptYkFDZkJmNTVSWSIsInNjb3BlIjoicmVhZDppbmRleC10aWNrZXIgcmVhZDpyYW5raW5nIHJlYWQ6bXdhIHJlYWQ6Z3dhIHJlYWQ6YWdncmVnYXRlcyByZWFkOm1hcmtldCByZWFkOmFzc2V0IHJlYWQ6b2hsY3YgcmVhZDptYXJrZXQtY2FwIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.DbT01troZgmmMrT5MZn_SZdTHgLEBKrKuX0pKELWKrT7y0JXl_MnA73ihZp2oAGpeNSm8VaENYBtVMg2-mPEc4_HTm0hx4dCHT_KHOxwoR7tH9RLIW-8FxVdf8W2jQyfdM3F7zIiojdnCDqbFI7zk8iHDFQnXu0tXleyv5PANTIe8gBHOff-mPf73Azhcc_yKkt2RKhgEx5tfaeeUsHxTaFF4amiP8g4tMOQOtUR0zJV-GCCOpK1mnhehrnDPGt6hCG_b5V7YS26JDl8je0-sHVKjJHePgncLPVf_KYW9P-b6Yf1wo3qRZ_HvxKzSbVykjA6QP_UQJK-hyDNYZoi5A",
        'X-RapidAPI-Key': "e8b084849bmsh343f37208c887aep14de33jsne3c62f3d595f",
        'X-RapidAPI-Host': "bravenewcoin.p.rapidapi.com"
    }
    conn = http.client.HTTPSConnection("bravenewcoin.p.rapidapi.com")
    conn.request("GET", "/market-cap?assetId=bf43c7fb-601e-41b0-96f1-34fad269cb13", headers=headers)

    res = conn.getresponse()

    data = res.read()
    print(data.decode("utf-8"))
    return render_template("bitcoin.html", bitcoinData=data.decode("utf-8"))


@app.route("/")
def get_movies():
    url = "https://api.themoviedb.org/3/discover/movie?api_key={}".format(os.environ.get("TMDB_API_KEY"))

    response = urllib.request.urlopen(url)
    data = response.read()
    dict = json.loads(data)

    return render_template("movies.html", movies=dict["results"])


@app.route("/hello")
def hello_world():
    return "<p>Hello, World!</p>"


if __name__ == '__main__':
    app.run(debug=True)
