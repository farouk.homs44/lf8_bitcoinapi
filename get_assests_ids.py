import http.client

conn = http.client.HTTPSConnection("bravenewcoin.p.rapidapi.com")

headers = {
    'X-RapidAPI-Key': "e8b084849bmsh343f37208c887aep14de33jsne3c62f3d595f",
    'X-RapidAPI-Host': "bravenewcoin.p.rapidapi.com"
    }

conn.request("GET", "/asset?status=ACTIVE", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))