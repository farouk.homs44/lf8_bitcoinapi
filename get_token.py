import http.client

conn = http.client.HTTPSConnection("bravenewcoin.p.rapidapi.com")

payload = "{\n    \"audience\": \"https://api.bravenewcoin.com\",\n    \"client_id\": \"oCdQoZoI96ERE9HY3sQ7JmbACfBf55RY\",\n    \"grant_type\": \"client_credentials\"\n}"

headers = {
    'content-type': "application/json",
    'X-RapidAPI-Key': "e8b084849bmsh343f37208c887aep14de33jsne3c62f3d595f",
    'X-RapidAPI-Host': "bravenewcoin.p.rapidapi.com"
}

conn.request("POST", "/oauth/token", payload, headers)

res = conn.getresponse()
data = res.read()

print(data)
